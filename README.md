# UOE Prototype Starter

These files are the framework used to create prototypes for UOE site designs.

Currently it uses
- Bootstrap v5.3.2 (https://getbootstrap.com/)
- SCSS
- PHP
- Codekit (To compile SCSS on local machine)

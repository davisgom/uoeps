<footer class="uoe-footer uoe-footer-dark">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-6 col-xxl-5 mb-4 mb-lg-0">
        <h2 class="uoe-footer-logo"><span class="visually-hidden">University Outreach and Engagement</span></h2>

        <p class="mb-0">
        The Office of University Outreach and Engagement facilitates university-wide efforts by supporting the engaged activities of faculty, staff, and students; fostering public access to university expertise and resources; and by advocating for exemplary engaged scholarship.
        </p>
      </div>

      <div class="col-12 col-xl-2">
      </div>
      
      <div class="col-12 col-xl-3 align-items-center">

	  <h3>UOE Links</h3>

          <ul class="align-self-end uoe-quick-links mb-0">
  						<li role="none" class="">
  							<a href="#" role="menuitem">Overview</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">UOE Departments</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">UOE Projects</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">Looking for Partners?</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">Public Events and Programs</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">UOE People</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">Contact</a>
  						</li>
  				</ul>
        </div>

		<div class="social-footer col-12 col-xl-auto">
			
				<h3>UOE Social Links</h3>

				<ul class="spaced-list mb-0" style="columns: 1;">
					<li>			
						<a href="https://www.facebook.com/msuengage" class="" role="menuitem" target="_blank">
							<img src="/Content/Images/social/facebook.svg">
							UOE Facebook
						</a>
					</li>

					<li>
						<a href="https://www.instagram.com/msuengage/" class="" role="menuitem" target="_blank">
							<img src="/Content/Images/social/instagram.svg">
							UOE Instagram
						</a>
					</li>
					
					<li>
						<a href="#" class="" role="menuitem">
							<img src="/Content/Images/social/linkedin.svg">
							UOE LinkedIn
						</a>
					</li>

					
					<li>
						<a href="https://www.youtube.com/user/uoecit" class="" role="menuitem" target="_blank">
							<img src="/Content/Images/social/youtube.svg">
							UOE Youtube
						</a>
					</li>
					
					<li>
						<a href="https://twitter.com/MSU_UOE" class="" role="menuitem" target="_blank">
							<img src="/Content/Images/social/x-twitter.svg">
							UOE Twitter
						</a>
					</li>

					<li>
						<a href="#" class="" role="menuitem">
							<img src="/Content/Images/social/envelope.svg">
							UOE Newsletter
						</a>
					</li>
				</ul>
			</div>

    </div>
  </div>
</footer>
<button class="menu-btn btn btn-light d-md-block d-lg-none d-print-none" onclick="navToggle()" onkeydown="navToggle()">
  <span class="menu-icon">
    <svg xmlns="http://www.w3.org/2000/svg" role="img" width="16" height="16" fill="currentColor" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M0 96C0 78.3 14.3 64 32 64H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 128 0 113.7 0 96zM0 256c0-17.7 14.3-32 32-32H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32zM448 416c0 17.7-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32H416c17.7 0 32 14.3 32 32z"/></svg>
    
    <br /> <span>Menu</span>
  </span>
  <span class="search-icon">
    <svg xmlns="http://www.w3.org/2000/svg" role="img" aria-label="[title]" width="20" height="20" fill="currentColor" viewBox="0 0 512 512"><title>Search</title><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z"/></svg>
  </span>
</button>

<section id="siteNav" class="overlay">
  <div class="overlay-content">
    <div class="container">
      <a class="closebtn d-lg-none" href="javascript:void(0)" onclick="navToggle()">
      <svg xmlns="http://www.w3.org/2000/svg" role="img" width="16" height="16" fill="currentColor" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M242.7 256l100.1-100.1c12.3-12.3 12.3-32.2 0-44.5l-22.2-22.2c-12.3-12.3-32.2-12.3-44.5 0L176 189.3 75.9 89.2c-12.3-12.3-32.2-12.3-44.5 0L9.2 111.5c-12.3 12.3-12.3 32.2 0 44.5L109.3 256 9.2 356.1c-12.3 12.3-12.3 32.2 0 44.5l22.2 22.2c12.3 12.3 32.2 12.3 44.5 0L176 322.7l100.1 100.1c12.3 12.3 32.2 12.3 44.5 0l22.2-22.2c12.3-12.3 12.3-32.2 0-44.5L242.7 256z"/></svg>CLOSE
      </a>

      <div id="MSUSearchTool" class="no-fill d-lg-none" role="search">
    			<?php include("msu-search.php"); ?>
    	</div>

      <nav class="row">
        <h1 class="visually-hidden">Site Navigation</h1>
        <ul class="nav nav-pills nav-fill" role="navigation">
            <li>
              <a href="home">
                Home
              </a>
            </li>

            <li>
              <a <?php if ($page_content == "about") {echo 'class="active"';} ?> href="about">
                About
              </a>
            </li>

            <li>
              <a <?php if ($page_content == "footers") {echo 'class="active"';} ?> href="footers">
                Footers
              </a>
            </li>
        </ul>
      </nav>
    </div>
  </div>

  <div class="container closebtn-bottom d-lg-none">
		<a class="closebtn" href="javascript:void(0)" onclick="navToggle()">
    <svg xmlns="http://www.w3.org/2000/svg" role="img" width="16" height="16" fill="currentColor" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M242.7 256l100.1-100.1c12.3-12.3 12.3-32.2 0-44.5l-22.2-22.2c-12.3-12.3-32.2-12.3-44.5 0L176 189.3 75.9 89.2c-12.3-12.3-32.2-12.3-44.5 0L9.2 111.5c-12.3 12.3-12.3 32.2 0 44.5L109.3 256 9.2 356.1c-12.3 12.3-12.3 32.2 0 44.5l22.2 22.2c12.3 12.3 32.2 12.3 44.5 0L176 322.7l100.1 100.1c12.3 12.3 32.2 12.3 44.5 0l22.2-22.2c12.3-12.3 12.3-32.2 0-44.5L242.7 256z"/></svg>CLOSE
    </a>
	</div>
</section>
<footer class="uoe-footer uoe-footer-dark">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-5 mb-4">
        <h2 class="uoe-footer-logo"><span class="visually-hidden">University Outreach and Engagement</span></h2>

        <p>
        The Office of University Outreach and Engagement facilitates university-wide efforts by supporting the engaged activities of faculty, staff, and students; fostering public access to university expertise and resources; and by advocating for exemplary engaged scholarship.
        </p>
      </div>

      <div class="col-12 col-lg-4">
		&nbsp;
      </div>
      
      <div class="col-12 col-lg-auto align-items-center">

	  <h3>UOE Links</h3>

          <ul class="align-self-end uoe-quick-links">
  						<li role="none" class="">
  							<a href="#" role="menuitem">Overview</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">UOE Departments</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">UOE Projects</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">Looking for Partners?</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">Public Events and Programs</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">UOE People</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">Contact</a>
  						</li>
  				</ul>
        </div>

    </div>
  </div>
</footer>
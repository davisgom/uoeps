<?php ob_start(); include ("Content/Pages/$page_content.php"); $content = ob_get_clean(); ?>

<?php if (isset($page_title)){ $set_page_title = $page_title;}?>

<header class="page-header <?php echo $page_content . '-header'; ?>">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-sm-9 d-flex">
        <h1>
          <span class="page-title">
            <?php if (isset($page_title)){ echo $page_title; } else echo $page_content; ?>
          </span>
        </h1>
      </div>
    </div>
</header>

<?php
if ($page_content == "footers") {
  echo $content;
} else {?>

<section class="container">
  <div class="row">
    <div class="col-11">
      <?php echo $content ?>
    </div>
  </div>
</section>

<?php } ?>